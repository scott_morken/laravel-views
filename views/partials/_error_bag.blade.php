<?php
/**
 * @var \Illuminate\Support\ViewErrorBag $errors
 * @var \Illuminate\Contracts\Support\MessageBag $bag
 */
?>
@if (isset($errors))
    @foreach ($errors->getBags() as $bag)
        @foreach ($bag->toArray() as $type => $messages)
            @include('smorken/views::partials.flash', ['message' => $messages])
        @endforeach
    @endforeach
@endif
