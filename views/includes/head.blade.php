<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="csrf-token" content="<?php echo csrf_token(); ?>">
<title>
    @section('title')
        {{ config('app.title', 'My App') }}
    @show
</title>
@if (file_exists(__DIR__ . '/favicon.ico'))
    <link rel="icon" href="{{ asset('/favicon.ico') }}" sizes="16x16 32x32 48x48 64x64" type="image/vnd.microsoft.icon">
@endif
@section('style')
        <!-- style -->
<link rel="stylesheet" type="text/css" href="{!! asset('css/app.css') !!}">
<!-- shim/respond for > ie 9 -->
@show

@section('head-js')
@show
