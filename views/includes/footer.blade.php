<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/19/14
 * Time: 9:32 AM
 */
?>
@if (Session::has('impersonating'))
    <div class="alert alert-warning"><strong>Impersonating: </strong>{{ Session::get('impersonating') }}</div>
@endif
