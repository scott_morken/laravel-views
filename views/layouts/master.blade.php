<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    @include('smorken/views::includes.head')
    @stack('header_scripts')
</head>
<body>
<div class="container">
    <div class="loading"></div>
    <header>
        @include('smorken/views::includes.header')
    </header>
    <!--[if lt IE 8]>
    <p class="alert alert-warning">You are using an <strong>outdated</strong> browser. Please <a
            href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    @if (class_exists('HTML'))
        {!! HTML::messages() !!}
    @else
        @include('smorken/views::partials._error_bag')
    @endif
    <div id="content">
        @yield('content')
    </div>
</div>
<footer class="footer">
    @include('smorken/views::includes.footer')
</footer>
@include('smorken/views::partials._modal')
@include('smorken/views::includes.foot')
@stack('footer_scripts')
</body>
</html>
