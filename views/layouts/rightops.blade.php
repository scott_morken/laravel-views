<?php $layout = isset($layout) ? $layout : 'smorken/views::layouts.master'; ?>
@extends($layout)
@section('content')
    <div class="row">
        <div id="sidebar-content" class="col-lg-10">
            @yield('content')
        </div>
        <div id="sidebar" class="col-lg-2">
            @section('operations')
                @if (isset($operations) && class_exists('HTML'))
                    <div class="operations well">
                        <div class="ops-header">Operations</div>
                        {!! HTML::operations($operations) !!}
                    </div>
                @endif
            @show
        </div>
    </div>
@overwrite
