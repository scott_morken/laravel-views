<?php $layout = isset($layout) ? $layout : 'smorken/views::layouts.master'; ?>
@extends($layout)
@section('content')
    <div id="sidebar" class="col-md-4">
        @include('smorken/views::includes.sidebar')
    </div>
    <div id="sidebar-content" class="col-md-8">
        @yield('content')
    </div>
@stop
