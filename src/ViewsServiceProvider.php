<?php namespace Smorken\Views;

use Illuminate\Support\ServiceProvider;

class ViewsServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'smorken/views');
        $this->publishes([__DIR__ . '/../views' => base_path('resources/views/vendor/smorken/views')]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
